#!/usr/bin/env python

from ctypes.util import find_library

from ._gcrypt import ffi
from . import errors
from .types.sexpression import SExpression
from .types.ec_point import Point
from .types.mpi import MPIint

lib = ffi.dlopen(find_library("gcrypt"))

class ECurve(object):
    """
    This object describes an elliptic curve. It provides methods to access points
    and mpi associated to it.

    It provides dictionnary like method to access to its member.
    """
    def __init__(self, keyparam=None, curve=None):
        """
        Let's create the context needed for the Elliptic curve calculation.
        It needs a keyparam formed like an ecc_keyparam S-Expression as a
        parameters and an optional curve-name to fill in the blank of the
        parameter.
        """
        # First we need to check if the keyparam is a valid S-Expression
        if keyparam is not None and not isinstance(keyparam, SExpression):
            raise TypeError("keyparam must be a SExpression. Got {} instead.".format(type(keyparam)))
        if keyparam is None and curve is None:
            raise TypeError("at least one of keyparam and curve must be passed to __init__")

        ec_ctx = ffi.new("gcry_ctx_t *", ffi.NULL)
        error = ffi.cast("gpg_error_t", 0)
        if keyparam is None:
            keyparam_t = ffi.NULL
        else:
            keyparam_t = keyparam.get()[0]

        error = lib.gcry_mpi_ec_new(ec_ctx, keyparam_t, curve.encode())
        if error != 0:
            raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

        self.__ctx = ec_ctx[0]
        self.name = curve

    def __del__(self):
        """
        We need to release the the context associated to ourself.
        """
        lib.gcry_ctx_release(self.__ctx)

    def get(self):
        """
        Let's access the pointer to our context, might be usefull
        """
        return self.__ctx

    def __setitem__(self, key, item):
        """
        We want to add either a Point or a MPI to our elliptic curve
        context.
        """
        error = ffi.cast("gcry_error_t", 0)

        if isinstance(item, Point):
            error = lib.gcry_mpi_ec_set_point(key.encode(), item.get(), self.get())
        elif isinstance(item, MPIint):
            error = lib.gcry_mpi_ec_set_mpi(key.encode(), item.get(), self.get())
        else:
            raise TypeError("Item must be Point or MPI. Got {} instead.".format(type(item)))

        if error != 0:
            raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

    def __getitem__(self, key):
        """
        Let's retrieve an item from the Elliptic Curve. Item to be retrieved can only be
        in keys defined by the ecc-private key params.

        p, a, b, n and d are mpi
        g and q are points.
        """
        if key in ['p', 'a', 'b', 'n', 'd']:
            # We want to have a mpi that can be copied
            mpi = MPIint()
            pointer = lib.gcry_mpi_ec_get_mpi(key.encode(), self.get(), 1) 
            if pointer == ffi.NULL:
                return None
            mpi.set(pointer)
            return mpi
        elif key in ['q', 'g']:
            # We want a point that can be copied
            point = Point(self.get())
            pointer = lib.gcry_mpi_ec_get_point(key.encode(), self.get(), 1)
            if pointer == ffi.NULL:
                return None
            point.set(pointer)
            return point
        else:
            raise KeyError


    def __repr__(self):
        """
        We want to print the current EC. For that we will create the S-Expression
        corresponding to the current state of the curve, and returns it in a tuple with the
        name of the curve.
        """
        return """(keyparam 
    (ecc 
        (p {})
        (a {})
        (b {})
        (n {})
        (d {})
        (q {})
        (g {})
    )
)""".format(self['p'], self['a'], self['b'], self['n'], self['d'], self['q'], self['g'])
