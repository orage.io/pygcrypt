#!/usr/bin/env python
from ctypes.util import find_library
from base64 import b64encode, b64decode
from functools import partial
from io import BytesIO

from ._gcrypt import ffi
from . import errors
from .types import mpi

lib = ffi.dlopen(find_library("gcrypt"))

def scan(in_bytes, mpi_fmt):
    """
    Convert the external representation of an integer stored in buffer with a
    length of buflen into a newly created MPI returned which will be stored
    at the address of r_mpi. For certain formats the length argument is
    not required and should be passed as 0. After a successful operation
    the variable nscanned receives the number of bytes actually scanned
    unless nscanned was given as NULL. format describes the format of the
    MPI as stored in buffer:

        GCRYMPI_FMT_STD
            2-complement stored without a length header. Note that
            gcry_mpi_print stores a 0 as a string of zero length.
        GCRYMPI_FMT_PGP
            As used by OpenPGP (only defined as unsigned). This is basically
            GCRYMPI_FMT_STD with a 2 byte big endian length header.
        GCRYMPI_FMT_SSH
            As used in the Secure Shell protocol. This is GCRYMPI_FMT_STD
            with a 4 byte big endian header.
        GCRYMPI_FMT_HEX
            Stored as a string with each byte of the MPI encoded as 2 hex
            digits. Negative numbers are prefix with a minus sign and in
            addition the high bit is always zero to make clear that an
            explicit sign ist used. When using this format, buflen must be
            zero.
        GCRYMPI_FMT_USG
            Simple unsigned integer.

    Note that all of the above formats store the integer in big-endian
    format (MSB first).

    in_bytes contains the bytes describing the object.
    mpi_fmt is actually a string with the value of STD, PGP, SSH, HEX or USG.

    we will return a MPI object corresponding to our scanned bytes.
    """
    error = ffi.cast("gcry_error_t", 0)
    fmt = getattr(lib, u'GCRYMPI_FMT_' + mpi_fmt)
    out_mpi = mpi.MPIint()
    length = ffi.cast("size_t", len(in_bytes))

    error = lib.gcry_mpi_scan(ffi.cast("gcry_mpi_t *", out_mpi.get()), fmt, in_bytes, length, ffi.NULL)
    if error > 0:
        raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

    return out_mpi

def mpi_print(mpi, mpi_fmt):
    """
    Convert the MPI a into an external representation described by format 
    (see above) and store it in a newly allocated buffer which address
    will be stored in the variable buffer points to. The number of bytes
    stored in this buffer will be stored in the variable nbytes points to,
    unless nbytes is NULL.

    Even if nbytes is zero, the function allocates at least one byte and
    store a zero there. Thus with formats GCRYMPI_FMT_STD and
    GCRYMPI_FMT_USG the caller may safely set a returned length of 0 to 1
    to represent a zero as a 1 byte string.
    """
    # We will first change the format
    mpi.__fmt = mpi_fmt
    return mpi.__repr__()

def swap(u, w):
    """
    Swap the values of a and b.
    Return a tuple of the swapped values.
    """
    lib.gcry_mpi_swap(u.get(), w.get())
    return(u, w)

def snatch(w, u):
    """
    Set u into w and release u. If w is NULL only u will be released. 
    """
    lib.gcry_mpi_snatch(u.get(), w.get())
    return u

def isneg(w):
    """
    Test if the mpi w is negative. Return true if w is negative, return false if it's positive or 0
    """
    ret = lib.gcry_mpi_is_neg(ffi.cast("const gcry_mpi_t", w.get()))
    if ret == 1:
        return True
    return False

def randomize(length, strength='STRONG'):
    """
    Set the multi-precision-integers w to a random non-negative number of
    nbits, using random data quality of level level. In case nbits is not a
    multiple of a byte, nbits is rounded up to the next byte boundary. When
    using a level of GCRY_WEAK_RANDOM this function makes use of
    gcry_create_nonce. 
    """
    w = mpi.MPIint()
    lib.gcry_mpi_randomize(w.get(), length, getattr(lib, 'GCRY_{}_RANDOM'.format(strength)))
    return w

def crc24(octets):
    """
    We need this function to work with armored things to check they are ok, or
    to compute the crc by itself.

    original C code is:

          #define CRC24_INIT 0xB704CEL
          #define CRC24_POLY 0x1864CFBL

          typedef long crc24;
          crc24 crc_octets(unsigned char *octets, size_t len)
          {
              crc24 crc = CRC24_INIT;
              int i;
              while (len--) {
                  crc ^= (*octets++) << 16;
                  for (i = 0; i < 8; i++) {
                      crc <<= 1;
                      if (crc & 0x1000000)
                          crc ^= CRC24_POLY;
                  }
              }
              return crc & 0xFFFFFFL;
          }
    """
    INIT = 0xB704CE
    POLY = 0x1864CFB
    crc = INIT
    for octet in octets: 
        crc ^= (octet << 16)
        for i in range(8):
            crc <<= 1
            if crc & 0x1000000: crc ^= POLY
    return crc & 0xFFFFFF

def r64encode(octets):
    """
    This function encode a binary content in a radix64 encoded
    message. Following teh RFC 4880, the output is splitted in
    lines of 74 characters, the CRC is added on its own line.

    This function does not build the full armor, it justs convert
    to Radix-64
    """
    out = b64encode(octets)
    lines = [l for l in iter(partial(BytesIO(out).read, 76), b'')]
    crc = crc24(octets)
    lines += [b'='+b64encode(str(crc).encode())]
    return b"\n".join(lines)

def r64decode(radix):
    """
    This function decode a binary content from a radix64 encoded
    message to a binary one. It also checks the crc of the decoded
    message and check it against the crc found in radix. The input
    is a previously r64encoded object, and so is split in lines of
    74char.

    This function does nothing about the armor. It just convert from
    Radix-64
    """
    #CRC is located after the base64 string
    out = b''
    for line in radix.splitlines():
        if line.startswith(b'='):
            # This line is the CRC one.
            # There is nothing more to read here
            crc_found = int(b64decode(line[1:]))
            break
        out += line
    out = b64decode(radix)
    crc_check = crc24(out)
    if crc_found != crc_check:
        raise TypeError("CRC does not match")

    return out
