#!/usr/bin/env python
from ctypes.util import find_library

from ._gcrypt import ffi
from . import errors
from . import ciphers
from .types import mpi, sexpression, key

lib = ffi.dlopen(find_library("gcrypt"))

class Context(object):
    """
    This class is used to contyrol and manage the gcrypt function.
    No operation can be done if the context has not been created
    and initialized.

    Parametrisations like multithreading and secure memory is done here.
    The parameters of the cipher used by this context is also done through
    methods linked here.

    Parameters of the gcrypt lib can be __set__ and __get__ from here, as if
    it would have been a dictionnary. Exceptions will be raised if parameters
    are accessed out of scopes.

    Most of the operations done here are described in the documentation:
      https://www.gnupg.org/documentation/manuals/gcrypt/Preparation.html#Preparation

    The Context class provide a member cipher which is a Cipher object, getting initialised
    at start and cleaned at stop.

    The cipher receives its parameters from the Context class (like key or IV and the like)
    """

    def __init__(self, secmem=True, hw_disabled=[]):
        """
        We want to initialise our application.

        secmem is a boolean used to activate the use of secmem. Default to True
        hw_disabled is a list of hardware feature we want to explictly disable for any reason.
              /* Version check should be the very first call because it
                 makes sure that important subsystems are initialized. */
              if (!gcry_check_version (GCRYPT_VERSION))
                {
                  fputs ("libgcrypt version mismatch\n", stderr);
                  exit (2);
                }

              /* We don't want to see any warnings, e.g. because we have not yet
                 parsed program options which might be used to suppress such
                 warnings. */
              gcry_control (GCRYCTL_SUSPEND_SECMEM_WARN);

              /* ... If required, other initialization goes here.  Note that the
                 process might still be running with increased privileges and that
                 the secure memory has not been initialized.  */

              /* Allocate a pool of 16k secure memory.  This make the secure memory
                 available and also drops privileges where needed.  */
              gcry_control (GCRYCTL_INIT_SECMEM, 16384, 0);

              /* It is now okay to let Libgcrypt complain when there was/is
                 a problem with the secure memory. */
              gcry_control (GCRYCTL_RESUME_SECMEM_WARN);

              /* ... If required, other initialization goes here.  */

              /* Tell Libgcrypt that initialization has completed. */
              gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);
        """

        # Let's parse through the feature we do not want.
        # hw_disabled is a list of unicode. We must encode them first
        # then call the gcry_control function related to it.
        # We also need to check, each time, if there's an error at some point
        error = ffi.cast("gcry_error_t", 0)
        for hw_feature in hw_disabled:
            error = lib.gcry_control(lib.GCRYCTL_DISABLE_HWF, ffi.new("const char []", hw_feature.encode()), ffi.NULL)
            if error != 0:
                raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

        # Then, let's check the version. This call is used to initialize the blibrary
        version = lib.gcry_check_version(ffi.new("const char []", b"1.6.3"))
        if version == ffi.NULL:
            # Something wrong happened
            raise Exception("Cannot initialize gcrypt")

        # So now, we have our version (woot) and initialised the library.
        # Need to check if we want secmem or not. 

        if secmem:
            error = lib.gcry_control(lib.GCRYCTL_SUSPEND_SECMEM_WARN)
            if error != 0:
                raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)
            error = lib.gcry_control(lib.GCRYCTL_INIT_SECMEM, ffi.cast("int", 16384), ffi.cast("int", 0)) # 16k of secmem shoudl be more than enough
            if error != 0:
                raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)
            error = lib.gcry_control(lib.GCRYCTL_RESUME_SECMEM_WARN)
            if error != 0:
                raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

        # And initialisation is now done
        error = lib.gcry_control(lib.GCRYCTL_INITIALIZATION_FINISHED, ffi.cast("int", 0))
        if error != 0:
            raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

    def __setitem__(self, item, value):
        """
        Let's define some parameters for the context or the cipher.
        """
        error = ffi.cast("gcry_error_t", 0)
        if item == 'cipher':
            if isinstance(value, ciphers.Cipher):
                self.cipher = value
            else:
                raise KeyError("{} must be an instance of ciphers.Cipher, {} given.".format(item, type(value)))
            return

        if item in ['key', 'iv', 'ctr']:
            # Let's define the key used by the cipher"
            if self.cipher:
                self.cipher[item] = value
            return

        raise KeyError("Cannot set {}.".format(item))

    def __getitem__(self, item):
        """
        Let's get a specific item for the class
        """
        if item == 'version':
            return ffi.string(lib.gcry_check_version(ffi.NULL)).decode()

        if item == 'algo':
            if self.cipher:
                return self.cipher['algo']
            else:
                raise KeyError("No ciphers initialized.")

        if item == 'mode':
            if self.cipher:
                return self.cipher.mode
            else:
                raise KeyError("No ciphers initailized.")

        if item in ['key', 'iv', 'ctr']:
            return getattr(self.cipher, item, None)

        if item == 'cipher':
            return getattr(self, cipher, None)

        raise KeyError("{} key not found".format(item))

    def mpi(self, obj):
        """
        This will generates a mpi and attributes it a class depending on the type of obj.
        """
        if isinstance(obj, int):
            # This is an int, so we want a MPIint
            return mpi.MPIint(obj)

        else:
            return mpi.MPIopaque(obj)

    def create_keys(self, algo=u'rsa', nbits=4096, **kwargs):
        """
        This method is used to generate a new keypair. The default are for a rsa:4096
        keypair.

        The function will first build a SExpression used to then generate the keypair,
        then return a tuple (private, public,) of the two generated key.

        More parameters can be given on invocation for advanced use and control (such as
        setting flags, the derive-params or derive parameters, etc)
        """

        # nbits must be a multiple of 8. Can be None if algo is ecc AND curve has been
        # given in kwargs
        if nbits == None and algo != u'ecc' and u'curve' not in kwargs:
            raise KeyError("nbits is None and we either are not in ecc algo, or no curve name has been given")

        if nbits != None and nbits % 8 != 0:
            raise KeyError("nbits should be a multiple of 8, got {} instead".format(nbits))

        # Algo must be one of rsa, dsa, elg or ecc
        if algo not in ['rsa', 'dsa', 'ecc', 'elg']:
            raise KeyError("algo is unknown. Got {}".format(algo))

        # Let's build the S-expression
        sexpr_string = '(genkey ({} '.format(algo)
        if nbits != None:
            sexpr_string += '(nbits {}:{})'.format(len(str(nbits)), nbits)
        for item in kwargs:
            if isinstance(kwargs[item], int):
                sexpr_string += '({} {}:{})'.format(item, len(str(kwargs[item])), kwargs[item])
            if isinstance(kwargs[item], mpi.MPI):
                sexpr_sttring += '({} #{:x}#)'.format(item, kwargs[item].value())
            if isinstance(kwargs[item], list):
                sexpr_string += '({} {})'.format(item, ' '.join(kwargs[item]))
            if isinstance(kwargs[item], sexpression.SExpression):
                sexpr_string += '({} {})'.format(item, kwargs[item])
            else:
                sexpr_string += '({} "{}")'.format(item, kwargs[item])
        keyParam = sexpression.SExpression(sexpr_string)

        # Let's generate the key
        error = ffi.cast("gcry_erro_t", 0)
        keys = ffi.new("gcry_sexp_t *")
        error = lib.gcry_pk_genkey(keys, keyParam.get()[0])
        if error != 0:
            raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)), error)

        # We now have the fnal SExpression
        keySexp = sexpression.SExpression(keys)

        # Let's split it in private and public keys
        private = keys.Key(keySexp['private-key'])
        public = keys.Key(keySexp['public-key'])
        return (private, public,)
