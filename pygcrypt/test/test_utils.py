#!/usr/bin/env python

import pytest

from pygcrypt import utils

def test_radix64():
    assert utils.r64decode(utils.r64encode(b'This is a wonderfull world')) == b'This is a wonderfull world'
