#!/usr/bin/env python

import pytest

from pygcrypt.types.sexpression import SExpression

def test_init(context):
    s_expr = SExpression(b'(a "123")')
    assert print(s_expr) == print(b'(1:a3:123)')
    s_expr = SExpression(b'(a %u b %s c %d)', 10, b'Hello World', 3, b'123')
    assert print(s_expr) == print(b'(a "3647627136" b #03# c "0")\n')

def test_getitem(context):
    s_expr = SExpression(b'(a "hello World")')
    assert print(s_expr['a']) == print("(hello World)")
    assert print(s_expr[0]) == print("(a)")
    assert print(s_expr[0:1]) == print(b'(a "hello World")')
    with pytest.raises(IndexError):
        s_expr[4]
        s_expr[-1]
        s_expr[3:5]

def test_carcdr(context):
    s_expr = SExpression(b'(tests (test "123") (test "456"))')
    assert print(s_expr.car()) == print(b'(tests)')
    assert print(s_expr.cdr()) == print(b'((test "123") (test "456"))')
    assert print(s_expr.cdr().car().car()) == print('b(test)')

def test_extract(context):
    s_expr = SExpression(b'(test (a "123") (b "-123"))')
    ret = s_expr.extract('a')
    assert ret['a'] == 3224115
    ret = s_expr.extract('-b', b'test')
    assert ret['b'] == 758198835
