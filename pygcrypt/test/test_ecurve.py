#!/usr/bin/env python

import pytest

from pygcrypt.ecurve import ECurve
from pygcrypt.types.mpi import MPIint

def test_get(context):
    ec = ECurve(curve='secp192r1')

    assert isinstance(ec['a'], MPIint)

def test_set(context):
    ec = ECurve(curve='secp192r1')

    a = ec['a'] * 2
    ec['a'] = a
    assert ec['a'] == a
