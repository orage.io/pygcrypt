#!/usr/bin/env python

import pytest

from pygcrypt.context import Context

@pytest.fixture
def context():
    return Context()
