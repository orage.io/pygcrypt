#!/usr/bin/env python
from ctypes.util import find_library

from .._gcrypt import ffi
from .. import errors
from .sexpression import SExpression

lib = ffi.dlopen(find_library("gcrypt"))

"""
This module provide the key object and the necessary function to
use them.

It does not provides encrypt and decrypt functions, nor sign/verifysign
which are provided in a higher level module.
"""

class Key(object):
    """
    This object will hold most of the key system.
    """
    def __init__(self, sexpr):
        """
        A key must be initiliased with a S-Expression.
        This S-expression defines algorithm, numbers
        and all other things to be used by it.
        """
        if not isinstance(sexpr, SExpression):
            raise TypeError("First parameters must be a SExpression, got {} instead".format(type(sexpr)))

        # Let's get the type of key we have
        try:
            sub_sexp = sexp['private-key']
            self.key_type = u'private'
            self.sexp = sub_sexp
        except KeyError:
            try:
                sub_sexp = sexp['public-key']
                self.key_type = u'public'
                self.sexp = sub_sexp
            except:
                raise TypeError("The provided S-Expression does not contains key data.")
        except:
            raise TypeError("The provided S-Expression does not contains key data.")

    def __len__(self):
        """
        Return what is commonly referred as the key length for the key
        """
        length = lib.gcry_pk_get_nbits(self.sexp.get())
        return length

    def __getitem__(self, item):
        error = ffi.cast("gcry_error_t", 0)
        if item == 'type':
            return getattr(self, key_type, None)

        if item == 'sexp':
            return getattr(self, sexp, None)

        if item == 'keylen':
            return len(self)

        if item == 'keygrip':
            # get the keygrip of the current key. If the 
            # algo does not support keygrip, it returns ffi.NULL
            # which is None
            ret = ffi.cast("unsigned char [20]", b'')
            ret = ffi.gc(ret, lib.gcry_free)
            lib.gcry_pk_get_keygrip(self.sexp.get(), ret)
            if ret == ffi.NULL:
                return None
            return ret

        if item == 'algo':
            # The algorithm is the cadr of the key.
            algo = self.sexp.cdr().car().getstring(0)
            # We want to check if this algo is available
            usable = lib.gcry_pk_test_algo(lib.gcry_pk_map_algo(algo))
            if usable != 0:
                raise Exception("Algorithm {} not usable on this platform")
            return algo.decode()

        if item == 'sign':
            # We want to know id this key can be used to sign.
            algo_int = lib.gcry_pk_map_name(self['algo'].encode())
            error = ffi.new("gcry_error_t", 0)
            error = lib.gcry_pk_algo_info(algo_int, lib.GCRYCTL_TEST_ALGO, ffi.NULL, ffi.cast("size_t *", lib.GCRY_PK_USAGE_SIGN))
            if error < 0:
                # We have an error
                raise Exception("Oops something went wrong")
            if error != 0:
                return False
            return True

        if item == 'encr':
            # We want to know id this key can be used to encrypt.
            algo_int = lib.gcry_pk_map_name(self['algo'].encode())
            error = ffi.new("gcry_error_t", 0)
            error = lib.gcry_pk_algo_info(algo_int, lib.GCRYCTL_TEST_ALGO, ffi.NULL, ffi.cast("size_t *", lib.GCRY_PK_USAGE_ENCR))
            if error < 0:
                # We have an error
                raise Exception("Oops something went wrong")
            if error != 0:
                return False
            return True

