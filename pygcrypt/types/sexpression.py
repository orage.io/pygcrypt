#!/usr/bien/env python
import re
from ctypes.util import find_library

from .._gcrypt import ffi
from .. import errors
from .mpi import MPIint, MPIopaque

lib = ffi.dlopen(find_library("gcrypt"))

class SExpression(object):
    """
    This class is used to manage S-Expression. They are list in lisp but
    generally the car (first element) is the type of the next item.

    It's extremely similar to a hashtable, so we're going to use the dict
    operators __getitem__, __setitem__ and __iter__ to reflect that.

    Howevern we will need some specific works to be done on the __repr__
    part since some specific formating is needed to use a S-expression in
    another one.
    """
    def __init__(self, *args, **kwargs):
        """
        Let's build a new S-Expression. We're going to use sscan to allow
        for detection of parsing errors.

        kwargs might contains various st of args, used to determine which
        function will be called to build the s-expr
        """
        error = ffi.cast("gcry_error_t", 0)
        self.__sexp = ffi.new("gcry_sexp_t *", ffi.NULL)
        self.__sexp = ffi.gc(self.__sexp, lib.gcry_sexp_release)
        erroffset = 0

        if len(args) == 0:
            raise TypeError("SExpresison must be created with at least 1 parameters, none given")

        if len(args) == 1 and isinstance(args[0], bytes):
            # We have only one arg, and it's a byte
            error = lib.gcry_sexp_sscan(self.__sexp, ffi.cast("size_t *", erroffset),  args[0], ffi.cast("size_t", len(args[0])))
        elif len(args) == 1 and (ffi.typeof(args[0]) is ffi.typeof("gcry_sexp_t *") or ffi.typeof(args[0]) is ffi.typeof("gcry_sexp_t")):
            self.set(args[0])
        else:
            # We need to work a little bit on the args passed to the varidadic part of the call
            vargs = []
            for arg in args[1:]:
                if isinstance(arg, bytes):
                    vargs.append(ffi.new("char []", arg))
                    break
                if isinstance(arg, int):
                    vargs.append(ffi.new("int []", arg))    
                    break
                vargs.append(arg)

            error = lib.gcry_sexp_build(self.__sexp, ffi.cast("size_t *", erroffset), args[0], *vargs)
        if error != 0:
            raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)
        self.__sexp = self.__sexp[0]
        self.__fmt = 'ADVANCED'

    def get(self):
        """
        Get the sexp object associated to this one
        """
        return self.__sexp

    def set(self, handle):
        """
        We want to setup the handle to an already existing in-memory SExpr
        """
        if self.__sexp != ffi.NULL:
            lib.gcry_sexp_release(self.__sexp)

        if ffi.typeof(handle) == ffi.typeof("gcry_sexp_t *"):
            self.__sexp = handle[0]
        elif ffi.typeof(handle) == ffi.typeof("gcry_sexp_t"):
            self.__sexp == handle
        else:
            raise TypeError("handle must be of type <ctype gcry_sexp_t> of <ctype gcry_sexp_t *> got {} instead".format(type(handle)))

    def fmt(self, fmt):
        """
        We want to define the format used by this s-expression.
        """
        self.__fmt = fmt

    def __len__(self):
        """
        Returns the len of the S-expression. It is defined as calling the sprint function but with
        a NULL buffer.
        """
        fmt = getattr(lib, u'GCRYSEXP_FMT_' + self.__fmt)
        return lib.gcry_sexp_sprint(self.get(), fmt, ffi.NULL, 0)

    def __repr__(self):
        """
        We need to print the S-expression in a format that can be used to be parsed
        into another S-expression.
        """
        fmt = getattr(lib, u'GCRYSEXP_FMT_' + self.__fmt)
        bytes_out = ffi.new("char [{}]".format(len(self)))
        size = lib.gcry_sexp_sprint(self.get(), fmt, bytes_out, len(self))
        return '{}'.format(ffi.string(bytes_out))

    def dump(self):
        """
        Dump the S-expression in a format suitable for libgcrypt debug mode
        """
        lib.gcry_sexp_dump(self.get())

    def __getitem__(self, key):
        """
        This is used to navigate through a S-expression, using token slice or indexes.
        """
        if isinstance(key, str):
            key = key.encode()

        if isinstance(key, bytes):
            sexp_match = lib.gcry_sexp_find_token(self.get(), key, 0)
            if sexp_match != ffi.NULL:
                return SExpression(b'(%S)', sexp_match)
            else:
                raise IndexError

        if isinstance(key, int):
            sexp_match = lib.gcry_sexp_nth(self.get(), key)
            if sexp_match != ffi.NULL:
                return SExpression(b'(%S)', sexp_match)
            else:
                raise IndexError

        if isinstance(key, slice):
            start = key.start
            stop = key.stop
            if not (key.step == 1 or key.step == None):
                raise IndexError
            if stop > len(self) or start > len(self):
                raise IndexError
            fmt = u' %S' * (stop - start)
            fmt = u'({})'.format(fmt)
            fmt = fmt.encode()
            args = [lib.gcry_sexp_nth(self.get(), i) for i in range(start, stop)]
            return SExpression(fmt, *args)

    def getstring(self, index):
        """
        This is used to get the string stored at index value in a SExpression.

        If the value can't be converted to a string or self[index] is another list, then
        we will return None
        """
        value = lib.gcry_sexp_nth_string(self.get(), index)
        value = ffi.gc(value, lib.gcry_free)
        if value == ffi.NULL:
            return None
        return ffi.string(value)

    def getmpi(self, index, fmt='USG'):
        """
        This is used to get the mpi stored at index value in a SExpression

        If the value can't be converted to a MPI or slef[index]  is another list, then
        we will return None
        """
        mpi = lib.gcry_sexp_nth_mpi(self.get(), index)
        mpi = ffi.gc(mpi, lib.gcry_mpi_release)
        if value == ffi.NULL:
            return None
        return MPIint().set(mpi)

    def car(self):
        """
        This method is used to return the car of a S-expression, which is the first element
        of the S-expression. It is generally the type of the S-expression.
        """
        return SExpression(b'(%S)', lib.gcry_sexp_car(self.get()))

    def cdr(self):
        """
        Return the other part of the list (the list, minus the car)
        """
        return SExpression(b'(%S)', lib.gcry_sexp_cdr(self.get()))

    def extract(self, params, path=None):
        """
        Extract parameters from an S-expression using a list of parameters name. It works in a simialr way to getopts.
        We must first parse the params string, in order to allocate enough mpi to hold the data.
        The result will be a dict of MPI mapped to their param name.

        If path is precised we will search for parameters inside a sub S-epression
        """
        # Let's get the params first, and build MPI and a pointer list.
        match = re.compile(r"([+-/&]?\w{1}|'\w+')")
        results = match.findall(params)
        result = {}
        pointers = []
        for item in results:
            if item.startswith('-'):
                result[item[1:]] = MPIint()
                result[item[1:]].fmt = 'STD'
            else:
                result[item] = MPIint()
                result[item].fmt = 'USG'
            pointers += [ffi.new("gcry_mpi_t *", ffi.NULL)]

        # The poin-ters list must end with a NULL pointer.
        pointers += [ffi.NULL]
        if path == None:
            path = ffi.NULL

        error = ffi.cast("gcry_error_t", 0)
        error = lib.gcry_sexp_extract_param(self.get(), path, params.encode(), *pointers)
        if error != 0:
            raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

        # Let's map the pointers back into MPI to have the correlation fixed
        i = 0
        for mpi in result:
            result[mpi].set(pointers[i][0])
            i+=1
        return result
