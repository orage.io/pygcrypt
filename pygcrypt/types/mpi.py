#!/usr/bin/env python
from ctypes.util import find_library
from sys import getsizeof
import struct

from .._gcrypt import ffi
from .. import errors

lib = ffi.dlopen(find_library("gcrypt"))

"""
This module implements the binding for gcrypt multiple
precision integer library needed for a lot of things.
"""

class MPI(object):
    """
    We want to bind into mpi lib gcrypt. This is a generic class that will be used
    as a mother class for specific MPI objects.
    """
    def __init__(self):
        raise NotImplemented

    def copy(self):
        """
        Create a new MPI as the exact copy of a but with the constant and
        immutable flags cleared.

        We will return the copy of the mpi, which will allow us to call this
        function the same way than dict.copy()
        """
        mpi = type(self)()
        mpi.set(lib.gcry_mpi_copy(ffi.cast("const gcry_mpi_t", self.get())))
        return mpi

    def get(self):
        """
        Return the MPI object of this MPI.
        """
        return self.mpi

    def set(self, mpi_data):
        """
        Assign the value of u to w and return w. If NULL is passed for w,
        a new MPI is allocated, set to the value of u and returned. 

        Set the inner pointer of __self.mpi to the new one passed as mpi_data
        """
        if ffi.typeof(mpi_data) != ffi.typeof("gcry_mpi_t"):
            raise TypeError("set must be passed a <ctype mpi_data_t> as arg, got {} instead".format(type(mpi_data)))
        self.mpi = lib.gcry_mpi_set(self.get(), mpi_data)
        self.mpi = ffi.gc(self.mpi, lib.gcry_mpi_release)

    def __dump(self):
        """
        Dump the value of a in a format suitable for debugging to Libgcrypt’s
        logging stream. Note that one leading space but no trailing space or
        linefeed will be printed. It is okay to pass NULL for a.
        """
        lib.gcry_mpi_dump(self.get()())

    def get_flag(self, flag):
        """
        This function is used to return the value of a specific flag.
        """
        ret = lib.gcry_mpi_get_flag(self.get(), getattr(lib, 'GCRYMPI_FLAG_' + flag))
        return bool(ret)

    def set_flag(self, flag, boolean):
        """
        This function is used to set or clear a specific flag of the MPI.
        """
        if boolean:
            if flag not in ['SECURE', 'IMMUTABLE', 'CONST']:
                raise TypeError("You cannot set {} on this MPI".format(flag))
            lib.gcry_mpi_set_flag(self.get(), getattr(lib, 'GCRYMPI_FLAG_' + flag))
        else:
            if flag not in ['IMMUTABLE']:
                raise TypeError("You cannot clear flag {} on this MPI".format(flag))
            lib.gcry_mpi_clear_flag(self.get(), getattr(lib, 'GCRYMPI_FLAG_' + flag))

    def __len__(self):
        """
        Returns the number of bits required to represent self
        """
        return lib.gcry_mpi_get_nbits(self.get())

    def __getitem__(self, index):
        """
        Return true if bit number n (counting from 0) is set in self, false if not.
        """
        return bool(lib.gcry_mpi_test_bit(self.get(), ffi.cast("unsigned int", index)))

    def __setitem__(self, index, bit):
        """
        Set the index bit of self to the value of the boolean in bit
        """
        if bit is True:
            lib.gcry_mpi_set_bit(self.get(), index)
        else:
            lib.gcry_mpi_clear_bit(self.get(), index)

    def set_highbit(self, index):
        """
        Set index bits and clear all the bits higher than index in self
        """
        lib.gcry_mpi_set_highbit(self.get(), index)

    def clear_highbit(self, index):
        """
        Clear bit at index position in self, as well as all higher bits
        """
        lib.gcry_mpi_clear_highbit(self.get(), index)

    def __lshift__(self, value):
        """
        Shift the value of self by value bits to the left.
        """
        tmp_mpi = type(self)()
        lib.gcry_mpi_lshift(tmp_mpi.get(), self.get(), value)
        return tmp_mpi

    def __ilshift__(self, value):
        return self.__lshift__(value)

    def __rshift__(self, value):
        """
        Shift the value of self by value bits to the right.
        """
        tmp_mpi = type(self)()
        lib.gcry_mpi_rshift(tmp_mpi.get(), self.get(), value)
        return tmp_mpi

    def __irshift__(self, value):
        return self.__rshift__(value)

    # Comparisons function can be done even with opaque MPI
    def __cmp__(self, other):
        """
        Do a comparison between a MPI and another number. Return 0 if both are equal,
        a negative number if self < other and a positive one if self > other.
        """
        if isinstance(other, MPI):
            return lib.gcry_mpi_cmp(self.get(), other.get())
        else:
            return lib.gcry_mpi_cmp_ui(self.get(), int(other))

    def __eq__(self, other):
        if self.__cmp__(other) == 0:
            return True
        return False

    def __gt__(self, other):
        if self.__cmp__(other) > 0:
            return True
        return False

    def __lt__(self, other):
        if self.__cmp__(other) < 0:
            return True
        return False

    def __ge__(self, other):
        if self.__cmp__(other) >= 0:
            return True
        return False

    def __le__(self, other):
        if self.__cmp__(other) <= 0:
            return True
        return False

    def value(self):
        """
        This will return the value of the MPI object as an int.
        """
        error = ffi.cast("gcry_error_t", 0)
        fmt = getattr(lib, u'GCRYMPI_FMT_' + self.fmt)
        size = ffi.new("size_t *", 0)
        error = lib.gcry_mpi_print(fmt, ffi.NULL, 0, size, self.get())
        if error > 0:
            raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

        out = ffi.new("unsigned char [{}]".format(size[0]), b'')
        error = lib.gcry_mpi_print(fmt, out, size[0], size, self.get())
        if error > 0:
            raise errors.GcryptException(ffi.string(lib.gcry_strerror(error)).decode(), error)

        value = 0
        for item in struct.unpack('{}B'.format(size[0]), ffi.buffer(out, size[0])):
            value <<= 8
            value ^= item
        if lib.gcry_mpi_is_neg(ffi.cast("const gcry_mpi_t", self.get())) == 1:
            return -value
        return value

class MPIint(MPI):
    """
    This class is used to manipulate classic numerical MPI - by opposition to the
    MPIobs which can handle anything.
    """
    def __init__(self, value=0, fmt=u'USG', flags=[]):
        """
        Allocate a new MPI object and initialize it to 0 and initially allocate
        enough memory for a number of at least size bits. This pre-allocation in only
        a small performance issue and not actually necessary because libgcrypt automatically
        re-allocates memory.
        The secure bit is used to create the MPI in secure memory area.
        The fmt string is used as the standard formatter for this specific MPI and defaults to
          the STD one.
        We set the MPI to the value of 'value' (defaults to 0)
        """
        value = int(value)
        if value < 0:
            raise ValueError("We need an unsigned int for value, not a signed one")

        self.mpi = lib.gcry_mpi_snew(ffi.cast("unsigned int", getsizeof(value) * 8)) if 'SECURE' in flags else lib.gcry_mpi_new(ffi.cast("unsigned int", getsizeof(value) * 8))
        self.mpi = self.gc(self.mpi, lib.gcry_mpi_release)
        self.fmt = fmt
        if value > 0:
            self.set_ui(value)

        for flag in flags:
            if flag != 'SECURE':
                # It has been done on initialisation
                self.set_flag(flag, True)

    def set_ui(self, u_int):
        """
        Assign the value of u to w and return w. If NULL is passed for w,
        a new MPI is allocated, set to the value of u and returned. This
        function takes an unsigned int as type for u and thus it is only
        possible to set w to small values (usually up to the word size of
        the CPU).
        """
        ret_mpi = lib.gcry_mpi_set_ui(self.get(), ffi.cast("unsigned int", u_int))
        self.set(ret_mpi)

    def __neg__(self):
        """
        Invert the value of a mpi.
        We first need to copy it, in order to modify it, and then we need
        to trash the copy.
        """
        tmp_mpi = self.copy()
        lib.gcry_mpi_neg(self.get(), tmp_mpi.get())
        return self

    def __abs__(self):
        """
        Clear the sign of this MPI
        """
        lib.gcry_mpi_abs(self.get())
        return self

    def __add__(self, other):
        """
        This is the addition operator. If other is an MPI we will call
        gcry_mpi_add. Otherwise we will cast and call gcry_mpi_add_ui.
        """
        ret_mpi = type(self)()
        if isinstance(other, MPI):
            lib.gcry_mpi_add(ret_mpi.get(), self.get(), other.get())
        else:
            value = ffi.cast("unsigned long", int(other))
            lib.gcry_mpi_add_ui(ret_mpi.get(), self.get(), value)
        return ret_mpi

    def __iadd__(self, increment):
        """
        This is the increment operator. increment it basically is the same as
        self.__add__(increment)
        """
        return self.__add__(increment)

    def addm(self, other, mod):
        """
        return self + other \\bmod mod
        """
        ret_mpi = type(self)()
        lib.gcry_mpi_addm(ret_mpi.get(), self.get(), other.get(), mod.get())
        return ret_mpi

    def __sub__(self, other):
        """
        This is the substraction operator. If other is an MPI, we will call
        gcry_mpi_sub. Otherwise, we will cast and call gcry_mpi_sub_ui.
        """
        ret_mpi = type(self)()
        if isinstance(other, MPI):
            lib.gcry_mpi_sub(ret_mpi.get(), self.get(), other.get())
        else:
            value = ffi.cast("unsigned long", int(other))
            lib.gcry_mpi_sub_ui(ret_mpi.get(), self.get(), value)
        return ret_mpi

    def __isub__(self, decrement):
        """
        This is the decrement operator. It basically is the same as
        self.__sub__(decrement)
        """
        return self.__sub__(decrement)

    def subm(self, other, mod):
        """
        return self - other \\bmod mod
        """
        ret_mpi = type(self)()
        lib.gcry_mpi_subm(ret_mpi.get(), self.get(), other.get(), mod.get())
        return ret_mpi

    def __mul__(self, other):
        """
        Casting as the ret of the fubctions.
        return self * other
        """
        ret_mpi = type(self)()
        if isinstance(other, MPI):
            lib.gcry_mpi_mul(ret_mpi.get(), self.get(), other.get())
        else:
            value = ffi.cast("unsigned long", int(other))
            lib.gcry_mpi_mul_ui(ret_mpi.get(), self.get(), other)
        return ret_mpi

    def __imul__(self, multiplier):
        """
        This is the self multiplying operator
        """
        return self.__mul__(multiplier)

    def mulm(self, other, mod):
        """
        return self * other \\bmod mod
        """
        ret_mpi = type(self)()
        lib.gcry_mpi_mulm(ret_mpi.get(), self.get(), other.get(), mod.get())
        return ret_mpi

    def mul2exp(self, exp):
        """
        return self * 2^exp
        """
        ret_mpi = type(self)()
        lib.gcry_mpi_mul_2exp(ret_mpi.get(), self.get(), ffi.cast("unsigned long", int(exp)))
        return ret_mpi

    def __floordiv__(self, divisor):
        """
        Castinbg as the rest of the function. The gcrypt functions returns both mod
        and dividend. This is a floordiv since we return a MPI which is an integer.
        return self // divisor
        """
        ret_mpi = type(self)()
        if not isinstance(divisor, MPI):
            _div = divisor
            divisor = type(self)()
            divisor.set_ui(int(_div))
        lib.gcry_mpi_div(ret_mpi.get(), ffi.NULL, self.get(), divisor.get(), 0)
        return ret_mpi

    def __ifloordiv__(self, divisor):
        return self.__floordiv__(divisor)

    def __mod__(self, divisor):
        """
        Return the modulus of self and divisor
        return self % divisor
        """
        ret_mpi = type(self)()
        if not isinstance(divisor, MPI):
            _div = divisor
            divisor = type(self)()
            divisor.set_ui(int(_div))
        lib.gcry_mpi_mod(ret_mpi.get(), self.get(), divisor.get())
        return ret_mpi

    def __imod__(self, divisor):
        return self.__mod__(divisor)

    def powm(self, other, mod):
        """
        return self ^ other \\bmod mod
        """
        ret_mpi = type(self)()
        lib.gcry_mpi_powm(ret_mpi.get(), self.get(), other.get(), mod.get())
        return ret_mpi

    def gcd(self, other):
        """
        Compute the greatest common divisor of self and other.
        """
        ret_mpi = type(self)()
        lib.gcry_mpi_gcd(ret_mpi.get(), self.get(), other.get())
        return ret_mpi

    def invm(self, other):
        """
        return the multipicative inverse of self \\bmod other if it
        exists.
        """
        ret_mpi = type(self)()
        lib.gcry_mpi_invm(ret_mpi.get(), self.get(), other.get())
        return ret_mpi

    def __repr__(self):
        """
        Convert the MPI a into an external representation described by format 
        (see above) and store it in a newly allocated buffer which address
        will be stored in the variable buffer points to. The number of bytes
        stored in this buffer will be stored in the variable nbytes points to,
        unless nbytes is NULL.

        Even if nbytes is zero, the function allocates at least one byte and
        store a zero there. Thus with formats GCRYMPI_FMT_STD and
        GCRYMPI_FMT_USG the caller may safely set a returned length of 0 to 1
        to represent a zero as a 1 byte string.

        This method is a short hand for utils.print(self, fmt=self.fmt)
        """
        return self.value().__repr__()
        #number = str(self.value())
        #length = len(number)
        #if length > 32:
        #    # If the number if big, we will just display the eight first and last bytes
        #    number = number[0:8] + '...' + number[-8:]
        #return '<MPI fmt={} value={}:{}>'.format(self.fmt, number, length)

class MPIopaque(MPI):
    """
    This class is used to store arbitrary (ie: not numbers) into an MPI.

    Math operations can't be done on it.
    """
    def __init__(self, value=b'', fmt=u'USG', flags=[]):
        """
        Allocate a new MPI object and initialize it to 0 and initially allocate
        enough memory for a number of at least size bits. This pre-allocation in only
        a small performance issue and not actually necessary because libgcrypt automatically
        re-allocates memory.
        The secure bit is used to create the MPI in secure memory area.
        The fmt string is used as the standard formatter for this specific MPI and defaults to
          the STD one.
        We set the MPI to the value of 'value' (defaults to b'')
        """
        self.mpi = lib.gcry_mpi_snew(ffi.cast("unsigned int", getsizeof(value) * 8)) if 'SECURE' in flags else lib.gcry_mpi_new(ffi.cast("unsigned int", getsizeof(value) * 8))
        self.mpi = self.gc(self.mpi, lib.gcry_mpi_release)
        self.fmt = fmt
        self.set_opaque(value)

        for flag in flags:
            if flag != 'SECURE':
                # It has been done on initialisation
                self.set_flag(flag, True)

    def set_opaque(self, opaque):
        """
        Set the value of the MPI to hold the arbitrary bit pattern to be
        a reflection of the python object opaque. The number of bits used
        bythe object is passed to the functions.
        """
        bits = getsizeof(opaque) * 8 # 1 byte = 8 bits
        dest_mpi = lib.gcry_mpi_set_opaque(self.get(), ffi.new_handle(opaque), bits)
        self.set(dest_mpi)

    def get_opaque(self):
        """
        Get back the value of the MPI opaque value and stores it in a python object.
        """
        if not self.get_flag('OPAQUE'):
            raise TypeError("This MPI is not opaque")

        bits = ffi.new("unsigned int *", 0)
        void = lib.gcry_mpi_get_opaque(self.get(), bits)
        opaque = ffi.from_handle(void)
        return opaque

    def __repr__(self):
        """
        We want to print the actual content of the MPI.
        It's mostly a get_opaque operation.
        """
        return get_opaque().__repr__()
